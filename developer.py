from tkinter import*
from tkinter import ttk
from turtle import bgcolor
from PIL import Image, ImageTk
from tkinter import messagebox
import mysql.connector
import cv2

class Developer:
    def __init__(self, root):
        self.root=root
        self.root.geometry("1530x790+0+0")
        self.root.title("face Recognition System")



        title_lbl=Label(self.root,text="Developer",font=("times new roman",25,"bold"),bg="white",fg="blue")
        title_lbl.place(x=(-80),y=20,width=1530,height=50)


   
        img_top=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\back.jpg")
        img_top=img_top.resize((1530,700),Image.ANTIALIAS)
        self.photoimg_top=ImageTk.PhotoImage(img_top)

        f_lbl=Label(self.root,image=self.photoimg_top)
        f_lbl.place(x=0,y=60,width=1530,height=720)

        #frame
        main_frame=Frame(f_lbl, bd=2, bg="skyblue",relief=RIDGE)
        main_frame.place(x=420,y=100,width=500,height=450)

        img_top1=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\a.jpg")
        img_top1=img_top1.resize((155,160),Image.ANTIALIAS)
        self.photoimg_top1=ImageTk.PhotoImage(img_top1)

        f_lbl=Label(main_frame,image=self.photoimg_top1,relief=RIDGE)
        f_lbl.place(x=5,y=5,width=155,height=160)

        #Developer information
        dev_lable=Label(main_frame,text="Name:Asha Khadka",font=("times new roman",16,"bold"),bg='white')
        dev_lable.place(x=165,y=10)

        dev_lable=Label(main_frame,text="Designation:Database Engineer",font=("times new roman",16,"bold"),bg='white')
        dev_lable.place(x=165,y=50)

        dev_lable=Label(main_frame,text="Company:Info Developers Pvt. Ltd.",font=("times new roman",16,"bold"),bg='white')
        dev_lable.place(x=165,y=90)

        dev_lable=Label(main_frame,text="Contact:9818120304",font=("times new roman",16,"bold"),bg='white')
        dev_lable.place(x=165,y=130)

        img_top2=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\bg2.gif")
        img_top2=img_top2.resize((488,270),Image.ANTIALIAS)
        self.photoimg_top2=ImageTk.PhotoImage(img_top2)

        f_lbl=Label(main_frame,image=self.photoimg_top2)
        f_lbl.place(x=5,y=170,width=488,height=270)



if __name__ == "__main__":
    root=Tk()
    object=Developer(root)
    root.mainloop()