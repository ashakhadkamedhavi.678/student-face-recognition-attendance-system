from tkinter import*
from tkinter import ttk
from PIL import Image, ImageTk
from tkinter import messagebox
import mysql.connector
import cv2
import os
import csv
from tkinter import filedialog


mydata=[]
class Attendance:
    def __init__(self, root):
        self.root=root
        self.root.geometry("1530x790+0+0")
        self.root.title("face Recognition System")
    
    #************varriable***********
        self.var_attend_id=StringVar()
        self.var_attend_roll=StringVar()
        self.var_attend_name=StringVar() 
        self.var_attend_dept=StringVar()
        self.var_attend_time=StringVar()
        self.var_attend_date=StringVar()
        self.var_attend_attendance=StringVar()


#Background Image
        img=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\back.jpg")
        img=img.resize((1530,790),Image.ANTIALIAS)
        self.photoimg=ImageTk.PhotoImage(img)

        bg_img=Label(self.root,image=self.photoimg)
        bg_img.place(x=0,y=0,width=1530,height=790)

        title_lbl=Label(bg_img,text="Student Attendance System",font=("times new roman",25,"bold"),bg="white",fg="blue")
        title_lbl.place(x=(-80),y=20,width=1530,height=45)
        
        #Left lable frame
        Left_frame=LabelFrame(bg_img,bd=3,bg="skyblue",relief=RIDGE,text="Student Attendance Details",font=("times new roman",25,"bold"),fg="white")
        Left_frame.place(x=50,y=210,width=600,height=340)

        left_inside_frame=LabelFrame(Left_frame,bd=2,bg="white",relief=RIDGE,text="Current Course Information",font=("times new roman",12,"bold" ))
        left_inside_frame.place(x=20,y=10,width=550,height=270)


        #Lable and Entry
        #attendance id
        AttendanceID_lable=Label(left_inside_frame,text="AttendanceId:",font=("times new roman",12,"bold"),bg="white")
        AttendanceID_lable.grid(row=0,column=0,padx=10,pady=10,sticky=W)
        
        AttendanceID_entry=ttk.Entry(left_inside_frame,width=14,textvariable=self.var_attend_id,font=("times row roman",13,"bold"))
        AttendanceID_entry.grid(row=0,column=1,pady=10,sticky=W)

        #Roll
        roll_lable=Label(left_inside_frame,text="Roll:",font=("times new roman",12,"bold"),bg="white")
        roll_lable.grid(row=0,column=2,padx=10,pady=10,sticky=W)
        
        attend_roll=ttk.Entry(left_inside_frame,width=14,textvariable=self.var_attend_roll,font=("times row roman",13,"bold"))
        attend_roll.grid(row=0,column=3,padx=10,pady=10,sticky=W)

        #Name
        namelable=Label(left_inside_frame,text="Name:",font=("times new roman",12,"bold"),bg="white")
        namelable.grid(row=1,column=0,padx=10,pady=10,sticky=W)

        attendname_entry=ttk.Entry(left_inside_frame,width=14,textvariable=self.var_attend_name,font=("times row roman",13,"bold"))
        attendname_entry.grid(row=1,column=1,pady=10,sticky=W)


        #Department
        deptlable=Label(left_inside_frame,text="Department:",font=("times new roman",12,"bold"),bg="white")
        deptlable.grid(row=1,column=2,padx=10,pady=10,sticky=W)
        
        attend_dept=ttk.Entry(left_inside_frame,width=14,textvariable=self.var_attend_dept,font=("times row roman",13,"bold"))
        attend_dept.grid(row=1,column=3,padx=10,pady=10,sticky=W)

        #Time
        timelable=Label(left_inside_frame,text="Time:",font=("times new roman",12,"bold"),bg="white")
        timelable.grid(row=2,column=0,padx=10,pady=10,sticky=W)

        attend_time=ttk.Entry(left_inside_frame,width=14,textvariable=self.var_attend_time,font=("times row roman",13,"bold"))
        attend_time.grid(row=2,column=1,pady=10,sticky=W)
        
        #Date
        datelable=Label(left_inside_frame,text="Date:",font=("times new roman",12,"bold"),bg="white")
        datelable.grid(row=2,column=2,padx=10,pady=10,sticky=W)

        attend_date=ttk.Entry(left_inside_frame,width=14,textvariable=self.var_attend_date,font=("times row roman",13,"bold"))
        attend_date.grid(row=2,column=3,padx=10,pady=10,sticky=W)
        
        #attendance
        attendancelable=Label(left_inside_frame,text="Attendance:",font=("times new roman",12,"bold"),bg="white")
        attendancelable.grid(row=3,column=0,padx=10,pady=10,sticky=W)

        self.attend_status=ttk.Combobox(left_inside_frame, width=14,textvariable=self.var_attend_attendance,font="comicsansns 11 bold",state="readonly")
        self.attend_status["values"]=("Status","Present","Absent")
        self.attend_status.grid(row=3,column=1,pady=8)
        self.attend_status.current(0)


        #buttons frame
        btn_frame=Frame(left_inside_frame,bd=2,relief=RIDGE)
        btn_frame.place(x=0,y=200,width=550,height=35)

        import_csv_btn=Button(btn_frame,text="Import csv",command=self.importCsv,width=17,font=("times new roman",13,"bold"),bg="blue",fg="white")
        import_csv_btn.grid(row=0,column=0)

        export_cvs_btn=Button(btn_frame,text="Export csv",command=self.exportCsv,width=17,font=("times new roman",13,"bold"),bg="blue",fg="white")
        export_cvs_btn.grid(row=0,column=1)

        self.attend_status=ttk.Combobox(left_inside_frame, width=14,font="comicsansns 11 bold",state="readonly")
        
        reset_btn=Button(btn_frame,text="Reset",command=self.reset_data,width=18,font=("times new roman",13,"bold"),bg="blue",fg="white")
        reset_btn.grid(row=0,column=3)

        
        #Right lable frame

        Right_frame=LabelFrame(bg_img,bd=3,bg="skyblue",relief=RIDGE,text="Attendance Details",font=("times new roman",25,"bold"),fg="white")
        Right_frame.place(x=700,y=210,width=600,height=340)

        right_inside_frame=LabelFrame(Right_frame,bd=2,bg="white",relief=RIDGE,font=("times new roman",12,"bold" ))
        right_inside_frame.place(x=20,y=10,width=550,height=270)

        #*********scroll bar table****************
        scroll_x=ttk.Scrollbar( right_inside_frame,orient=HORIZONTAL)
        scroll_y=ttk.Scrollbar( right_inside_frame,orient=VERTICAL)

        self.AttendanceReportTable=ttk.Treeview(right_inside_frame,column=("id","roll","name","department","time","date","attendance"),xscrollcommand=scroll_x.set,yscrollcommand=scroll_y.set)

        scroll_x.pack(side=BOTTOM,fill=X)
        scroll_y.pack(side=RIGHT,fill=Y)

        scroll_x.config(command=self.AttendanceReportTable.xview)
        scroll_y.config(command=self.AttendanceReportTable.yview)


        self.AttendanceReportTable.heading("id",text="Attendance ID")
        self.AttendanceReportTable.heading("roll",text="Roll")
        self.AttendanceReportTable.heading("name",text="Name")
        self.AttendanceReportTable.heading("department",text="Department")
        self.AttendanceReportTable.heading("time",text="Time")
        self.AttendanceReportTable.heading("date",text="Date")
        self.AttendanceReportTable.heading("attendance",text="Attendance")

        self.AttendanceReportTable["show"]="headings"

        self.AttendanceReportTable.column("id",width=100)
        self.AttendanceReportTable.column("roll",width=100)
        self.AttendanceReportTable.column("name",width=100)
        self.AttendanceReportTable.column("department",width=100)
        self.AttendanceReportTable.column("time",width=100)
        self.AttendanceReportTable.column("date",width=100)
        self.AttendanceReportTable.column("attendance",width=100)


        self.AttendanceReportTable.pack(fill=BOTH,expand=1)

        self.AttendanceReportTable.bind("<ButtonRelease>",self.get_cursor)

    #***********fetch date***************************
    def fetchData(self,rows):
        self.AttendanceReportTable.delete(*self.AttendanceReportTable.get_children())
        for i in rows:
            self.AttendanceReportTable.insert("",END,values=i)

        

    def importCsv(self):
        global mydata
        mydata.clear()
        fln=filedialog.askopenfilename(initialdir=os.getcwd(),title="Open CSV",filetypes=(("CSV File","*.csv"),("All File","*.*")),parent=self.root)
        with open(fln) as myfile:
            csvread=csv.reader(myfile,delimiter=",")
            for i in csvread:
                mydata.append(i)
            self.fetchData(mydata)

    def exportCsv(self):
        try:
            if len(mydata)<1:
                messagebox.showerror("No data","No Data found to export",parent=self.root)
                return False
            fln=filedialog.asksaveasfilename(initialdir=os.getcwd(),title="Open CSV",filetypes=(("CSV File","*.csv"),("All File","*.*")),parent=self.root)
            with open(fln,mode="w",newline="")as myfile:
                exp_write=csv.writer(myfile,delimiter=",")
                for i in mydata:
                   exp_write.writerow(i)
                messagebox.showinfo("Data Export","Your data exported to"+os.path.basename(fln)+"successfully")
        except Exception as es:
           messagebox.showerror("Error", f"Due to:{str(es)}",parent=self.root)


    def get_cursor(self,event=""):
        cursor_row=self.AttendanceReportTable.focus()
        content=self.AttendanceReportTable.item(cursor_row)
        rows=content['values']
        self.var_attend_id.set(rows[0])
        self.var_attend_roll.set(rows[1])
        self.var_attend_name.set(rows[2])
        self.var_attend_dept.set(rows[3])
        self.var_attend_time.set(rows[4])
        self.var_attend_date.set(rows[5])
        self.var_attend_attendance.set(rows[6])


    def reset_data(self):
        self.var_attend_id.set("")
        self.var_attend_roll.set("")
        self.var_attend_name.set("")
        self.var_attend_dept.set("")
        self.var_attend_time.set("")
        self.var_attend_date.set("")
        self.var_attend_attendance.set("")


if __name__ == "__main__":
    root=Tk()
    object=Attendance(root)
    root.mainloop()