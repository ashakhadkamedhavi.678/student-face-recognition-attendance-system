from tkinter import*
from tkinter import ttk
from turtle import update
from PIL import Image,ImageTk
from tkinter import messagebox
import mysql.connector
from main import Face_Recognition_System

def main():
    win=Tk()
    app=Login_Window(win)
    win.mainloop()


class Login_Window:
    def __init__(self,root):
        self.root=root
        self.root.title("login")
        self.root.geometry("1530x790+0+0")


        img_top=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\4431731.jpg")
        img_top=img_top.resize((1530,700),Image.ANTIALIAS)
        self.photoimg_top=ImageTk.PhotoImage(img_top)

        f_lbl=Label(self.root,image=self.photoimg_top)
        f_lbl.place(x=0,y=0,width=1530,height=720)


         #frame
        main_frame=Frame(f_lbl, bd=2, bg="black",relief=RIDGE)
        main_frame.place(x=490,y=120,width=640,height=450)

        img_top1=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\Login.png")
        img_top1=img_top1.resize((100,100),Image.ANTIALIAS)
        self.photoimg_top1=ImageTk.PhotoImage(img_top1)

        f_lbl=Label(main_frame,image=self.photoimg_top1,bg="black")
        f_lbl.place(x=120,y=10,width=100,height=100)

        login_label=Label(main_frame,text="Login",font=("times new roman",20,"bold"),bg='black',fg='purple')
        login_label.place(x=135,y=110)

        #Label
        
        username_label=Label(main_frame,text="Username",font=("times new roman",15,"bold"),bg='black',fg='purple')
        username_label.place(x=40,y=170)

        self.txtuser=ttk.Entry(main_frame,font=("times new roman",15,"bold"))
        self.txtuser.place(x=10,y=200,width=325)

          
        password_label=Label(main_frame,text="Password",font=("times new roman",15,"bold"),bg='black',fg='purple')
        password_label.place(x=40,y=250)

        self.txtpassword=ttk.Entry(main_frame,font=("times new roman",15,"bold"))
        self.txtpassword.place(x=10,y=280,width=325)

        img_top2=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\user.png")
        img_top2=img_top2.resize((25,25),Image.ANTIALIAS)
        self.photoimg_top2=ImageTk.PhotoImage(img_top2)

        f_lbl=Label(main_frame,image=self.photoimg_top2,bg="black")
        f_lbl.place(x=10,y=170,width=25,height=25)


        img_top3=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\password.png")
        img_top3=img_top3.resize((25,25),Image.ANTIALIAS)
        self.photoimg_top3=ImageTk.PhotoImage(img_top3)

        f_lbl=Label(main_frame,image=self.photoimg_top3,bg="black")
        f_lbl.place(x=10,y=250,width=25,height=25)

        #Login Button
        loginbtn=Button(main_frame,command=self.login,text="Login",font=("times new roman",15,"bold"),bd=3,relief=RIDGE,fg="white",bg="red",activeforeground="white",activebackground="red")
        loginbtn.place(x=110, y=330,width=120,height=35)

        #Registerbutton
        registerbtn=Button(main_frame,text="New User Register",command=self.register_window,font=("times new roman",10,"bold"),borderwidth=0,fg="purple",bg="black",activeforeground="white",activebackground="purple")
        registerbtn.place(x=15, y=380,width=120)

        #Forgetpassbtn
        forgetpasswordbtn=Button(main_frame,text="Forget Password",command=self.forget_password_window,font=("times new roman",10,"bold"),borderwidth=0,fg="purple",bg="black",activeforeground="white",activebackground="purple")
        forgetpasswordbtn.place(x=10, y=410,width=120)


        #*******Right image*************
        right_img=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\pexels-photo-568948.jpeg")
        right_img=right_img.resize((300,450),Image.ANTIALIAS)
        self.photoright_img=ImageTk.PhotoImage(right_img)

        f_lbl=Label(main_frame,image=self.photoright_img)
        f_lbl.place(x=340,y=0,width=295,height=445)



    def register_window(self):
        self.new_window=Toplevel(self.root)
        self.app=Register(self.new_window)

    def login(self):
        if self.txtuser.get()=="" or self.txtpassword.get()=="":
           messagebox.showerror("Error","all field required")
        elif self.txtuser.get()=="asha" and self.txtpassword.get()=="ashu":
            messagebox.showinfo("Success","Welcome to Face Recognition")
        else:
            conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
            my_cursor=conn.cursor() 
            my_cursor.execute("select * from register where email=%s and password=%s",(
                                                                                    self.txtuser.get(),
                                                                                    self.txtpassword.get()
                                                                            ))
            row=my_cursor.fetchone()
            #print(row)
            if row==None:
                messagebox.showerror("Error","Invalid Username and Password")
            else:
                open_main=messagebox.askyesno("YesNo","Acess only admin")
                if open_main>0:
                    self.new_window=Toplevel(self.root)
                    self.app=Face_Recognition_System(self.new_window)
                else:
                    if not open_main:
                        return
            conn.commit()
            conn.close()


#*************reset password*****************
    def reset_password(self):
        if self.combo_security_q.get=="Select":
            messagebox.showerror("Error","Select the security question",parent=self.root)
        elif self.security_a.get()=="":
            messagebox.showerror("Error","Please enter the answer",parent=self.root)
        elif self.newpassword.get()=="":
            messagebox.showerror("Error","Please enter the new password",parent=self.root)
        else:
            conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
            my_cursor=conn.cursor()
            query=("select * from register where email=%s and security_q=%s and security_a=%s")
            value=(self.txtuser.get(),self.combo_security_q.get(),self.security_a.get(),)
            my_cursor.execute(query,value)
            row=my_cursor.fetchone()
            if row==None:
                messagebox.showerror("Error","Please enter correct answer",parent=self.root)
            else:
                query=("update register set password=%s where email=%s")
                value=(self.newpassword.get(), self.txtuser.get())
                my_cursor.execute(query,value)

                conn.commit()
                conn.close()
                messagebox.showinfo("Info","Your password has been reset, please login new password",parent=self.root)
                self.root2.destroy()

#***************forget password window************
    def forget_password_window(self):
        if self.txtuser.get()=="":
            messagebox.showerror("Error","Please Enter the Email address to reset Password")
        else:
            conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
            my_cursor=conn.cursor()
            query=("select * from register where email=%s")
            value=(self.txtuser.get(),)
            my_cursor.execute(query,value)
            row=my_cursor.fetchone()
           # print(row)
            if row==None:
               messagebox.showerror("My Error","Please enter the valid user name")
            else:
                conn.close()
                self.root2=Toplevel()
                self.root2.title("Forget Password")
                self.root2.geometry("350x400+570+160")

                l=Label(self.root2,text="Forget Password",font=("times new roman",15,"bold"),fg="red",bg="white")
                l.place(x=0,y=10,relwidth=1)


                security_q_label=Label(self.root2,text="Select Security Question",font=("times new roman",15,"bold"),fg='purple')
                security_q_label.place(x=50,y=80)

                self.combo_security_q=ttk.Combobox(self.root2,font=("times new roman",15,"bold"),state="randomly")
                self.combo_security_q["values"]=("Select","Your Birth Place","Your Pet Name")
                self.combo_security_q.place(x=50,y=110,width=250)
                self.combo_security_q.current(0)


                
                security_a_label=Label(self.root2,text="Security Answer",font=("times new roman",15,"bold"),fg='purple')
                security_a_label.place(x=50,y=150)

                self.security_a=ttk.Entry(self.root2,font=("times new roman",15,"bold"))
                self.security_a.place(x=50,y=180,width=250)

                   
                new_password_label=Label(self.root2,text="Password",font=("times new roman",15,"bold"),fg='purple')
                new_password_label.place(x=50,y=220)

                self.newpassword=ttk.Entry(self.root2,font=("times new roman",15,"bold"))
                self.newpassword.place(x=50,y=250,width=250)


                btn=Button(self.root2,text="Reset",command=self.reset_password,font=("times new roman",15,"bold"),fg="white",bg="green")
                btn.place(x=130,y=310)



class Register:
    def __init__(self,root):
        self.root=root
        self.root.title("Register")
        self.root.geometry("1530x790+0+0")

    #**************variable*****
        self.var_fname=StringVar()
        self.var_lname=StringVar()
        self.var_contact=StringVar()
        self.var_email=StringVar()
        self.var_security_q=StringVar()
        self.var_security_a=StringVar()
        self.var_password=StringVar()
        self.var_confirm_password=StringVar()
        

#************bg image*********
        img_top=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\pexels-photo-1303081.jpeg")
        img_top=img_top.resize((1530,700),Image.ANTIALIAS)
        self.photoimg_top=ImageTk.PhotoImage(img_top)

        f_lbl=Label(self.root,image=self.photoimg_top)
        f_lbl.place(x=0,y=0,width=1530,height=720)



        #*******left image*************
        left_img=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\flower.webp")
        left_img=left_img.resize((470,550),Image.ANTIALIAS)
        self.photoleft_img=ImageTk.PhotoImage(left_img)

        f_lbl=Label(self.root,image=self.photoleft_img)
        f_lbl.place(x=50,y=100,width=400,height=550)


        
        #frame
        main_frame=Frame(self.root, bg="white",relief=RIDGE)
        main_frame.place(x=450,y=100,width=600,height=550)

         
        register_lbl=Label(main_frame,text="Register",font=("times new roman",20,"bold"),fg='purple')
        register_lbl.place(x=250,y=10)



        #***label and entry**********
        fname_label=Label(main_frame,text="Firstname",font=("times new roman",15,"bold"),fg='purple')
        fname_label.place(x=10,y=50)

        self.fname=ttk.Entry(main_frame,textvariable=self.var_fname,font=("times new roman",15,"bold"))
        self.fname.place(x=10,y=80,width=250)


        lname_label=Label(main_frame,text="Lastname",font=("times new roman",15,"bold"),fg='purple')
        lname_label.place(x=340,y=50)

        self.lname=ttk.Entry(main_frame,textvariable=self.var_lname,font=("times new roman",15,"bold"))
        self.lname.place(x=340,y=80,width=250)



    
        contact_label=Label(main_frame,text="Contact",font=("times new roman",15,"bold"),fg='purple')
        contact_label.place(x=10,y=140)

        self.contact=ttk.Entry(main_frame,textvariable=self.var_contact,font=("times new roman",15,"bold"))
        self.contact.place(x=10,y=170,width=250)


        
        email_label=Label(main_frame,text="Email",font=("times new roman",15,"bold"),fg='purple')
        email_label.place(x=340,y=140)

        self.email=ttk.Entry(main_frame,textvariable=self.var_email,font=("times new roman",15,"bold"))
        self.email.place(x=340,y=170,width=250)



        
        security_q_label=Label(main_frame,text="Select Security Question",font=("times new roman",15,"bold"),fg='purple')
        security_q_label.place(x=10,y=230)

        self.combo_security_q=ttk.Combobox(main_frame,textvariable=self.var_security_q,font=("times new roman",15,"bold"),state="randomly")
        self.combo_security_q["values"]=("Select","Your Birth Place","Your Pet Name")
        self.combo_security_q.place(x=10,y=260,width=250)
        self.combo_security_q.current(0)


        
        security_a_label=Label(main_frame,text="Security Answer",font=("times new roman",15,"bold"),fg='purple')
        security_a_label.place(x=340,y=230)

        self.security_a=ttk.Entry(main_frame,textvariable=self.var_security_a,font=("times new roman",15,"bold"))
        self.security_a.place(x=340,y=260,width=250)



        
        password_label=Label(main_frame,text="Password",font=("times new roman",15,"bold"),fg='purple')
        password_label.place(x=10,y=320)

        self.password=ttk.Entry(main_frame,textvariable=self.var_password,font=("times new roman",15,"bold"))
        self.password.place(x=10,y=350,width=250)


        
        confirm_password_label=Label(main_frame,text="Confirm Password",font=("times new roman",15,"bold"),fg='purple')
        confirm_password_label.place(x=340,y=320)

        self.confirm_password=ttk.Entry(main_frame,textvariable=self.var_confirm_password,font=("times new roman",15,"bold"))
        self.confirm_password.place(x=340,y=350,width=250)

        #**********checkbutton************
        self.var_checkbtn=IntVar()
        checkbtn=Checkbutton(main_frame,variable=self.var_checkbtn,text="I Agree The Terms & Condition", font=("times new roman",10,"bold"),fg='purple',onvalue=1,offvalue=0)
        checkbtn.place(x=10,y=390)


        #*********buttons****************
        img=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\register-now-button.jpg")
        img=img.resize((200,50),Image.ANTIALIAS)
        self.photoimage=ImageTk.PhotoImage(img)
        b1=Button(main_frame,image=self.photoimage,command=self.register_data,borderwidth=0,cursor="hand2",font=("times new roman",15,"bold"))
        b1.place(x=70,y=440,width=200)


        img1=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\login-now.jpg")
        img1=img1.resize((200,70),Image.ANTIALIAS)
        self.photoimage1=ImageTk.PhotoImage(img1)
        b2=Button(main_frame,image=self.photoimage1,command=self.return_login,borderwidth=0,cursor="hand2",font=("times new roman",15,"bold"))
        b2.place(x=290,y=450,width=210)

        #***************function declaration*************

    def register_data(self):
        if self.var_fname.get()==""or self.var_email.get()==""or self.var_security_q.get()=="Select":
            messagebox.showerror("Error","All fields are required")
        elif self.var_password.get()!=self.var_confirm_password.get():
            messagebox.showerror("Error","password & confirm password must be same")
        elif self.var_checkbtn.get()==0:
            messagebox.showerror("Error","Please agree our terms & condition")
        else:
            conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
            my_cursor=conn.cursor() 
            query=("select * from register where email=%s")
            value=(self.var_email.get(),)
            my_cursor.execute(query,value)
            row=my_cursor.fetchone()
            if row!=None:
                messagebox.showerror("Error","User already exist, please try another email")
            else:
                my_cursor.execute("insert into register values(%s,%s,%s,%s,%s,%s,%s)",(
                                                                                        self.var_fname.get(),
                                                                                        self.var_lname.get(),
                                                                                        self.var_contact.get(),
                                                                                        self.var_email.get(),
                                                                                        self.var_security_q.get(),
                                                                                        self.var_security_a.get(),
                                                                                        self.var_password.get()
                                                                                    ))
            conn.commit()
            conn.close()
            messagebox.showinfo("Success","Register Successfully")

    def return_login(self):
        self.root.destory()

if __name__ == "__main__":
    main()
        