from tkinter import*
from tkinter import ttk
import tkinter
from turtle import title
from PIL import Image, ImageTk
import os
from student import Student
from train import Train
from face_recognition import Face_Recognition
from attendance import Attendance
from developer import Developer
from help import Help

class Face_Recognition_System:
    def __init__(self, root):
        self.root=root
        self.root.geometry("1530x790+0+0")
        self.root.title("face Recognition System")

        #img=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\background-image.jpg")
        #img=img.resize((1530,790),Image.ANTIALIAS)
        #self.photoimg=ImageTk.PhotoImage(img)

        #f_lbl=Label(self.root,image=self.photoimg)
        #f_lbl.place(x=0,y=0,width=1530,height=790)
       
        #Background Image
        img=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\back.jpg")
        img=img.resize((1530,790),Image.ANTIALIAS)
        self.photoimg=ImageTk.PhotoImage(img)

        bg_img=Label(self.root,image=self.photoimg)
        bg_img.place(x=0,y=0,width=1530,height=790)

        title_lbl=Label(bg_img,text="STUDENT FACE RECOGNITION ATTENDANCE SYSTEM",font=("times new roman",25,"bold"),bg="white",fg="blue")
        title_lbl.place(x=(-70),y=20,width=1530,height=45)

        #Studen Button
        img1=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\Student-image.gif")
        img1=img1.resize((220,220),Image.ANTIALIAS)
        self.photoimg1=ImageTk.PhotoImage(img1)

        b1=Button(bg_img,image=self.photoimg1,command=self.student_details,cursor="hand2")
        b1.place(x=100,y=120,width=220,height=220)

        b1_1=Button(bg_img,text="Student Details",command=self.student_details,cursor="hand2",font=("times new roman",15,"bold"),bg="blue",fg="white")
        b1_1.place(x=100,y=320,width=220,height=25)


        #Face detect button
        img2=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\Face-detector-image.jpg")
        img2=img2.resize((220,220),Image.ANTIALIAS)
        self.photoimg2=ImageTk.PhotoImage(img2)

        b2=Button(bg_img,image=self.photoimg2,cursor="hand2",command=self.face_data)
        b2.place(x=400,y=120,width=220,height=220)

        b2_1=Button(bg_img,text="Face detector",cursor="hand2",command=self.face_data,font=("times new roman",15,"bold"),bg="blue",fg="white")
        b2_1.place(x=400,y=320,width=220,height=25)


        #Attendance Face button
        img3=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\Attendance-image.jpg")
        img3=img3.resize((220,220),Image.ANTIALIAS)
        self.photoimg3=ImageTk.PhotoImage(img3)

        b3=Button(bg_img,image=self.photoimg3,cursor="hand2",command=self.attendance_data)
        b3.place(x=700,y=120,width=220,height=220)

        b3_1=Button(bg_img,text="Attendance Face",cursor="hand2",command=self.attendance_data,font=("times new roman",15,"bold"),bg="blue",fg="white")
        b3_1.place(x=700,y=320,width=220,height=25)


        
        #Help Face button
        img4=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\Help-desk-image.jpg")
        img4=img4.resize((220,220),Image.ANTIALIAS)
        self.photoimg4=ImageTk.PhotoImage(img4)

        b4=Button(bg_img,image=self.photoimg4,cursor="hand2",command=self.help)
        b4.place(x=1000,y=120,width=220,height=220)

        b4_1=Button(bg_img,text="Help Desk",cursor="hand2",command=self.help,font=("times new roman",15,"bold"),bg="blue",fg="white")
        b4_1.place(x=1000,y=320,width=220,height=25)


        #Train Face button
        img5=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\Train-data-image.jpg")
        img5=img5.resize((220,220),Image.ANTIALIAS)
        self.photoimg5=ImageTk.PhotoImage(img5)

        b5=Button(bg_img,image=self.photoimg5,cursor="hand2",command=self.train_data)
        b5.place(x=100,y=400,width=220,height=220)

        b5_1=Button(bg_img,text="Train Data",cursor="hand2",command=self.train_data,font=("times new roman",15,"bold"),bg="blue",fg="white")
        b5_1.place(x=100,y=600,width=220,height=25)


        #Photos Face button
        img6=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\Photo1.png")
        img6=img6.resize((220,220),Image.ANTIALIAS)
        self.photoimg6=ImageTk.PhotoImage(img6)

        b6=Button(bg_img,image=self.photoimg6,cursor="hand2",command=self.open_img)
        b6.place(x=400,y=400,width=220,height=220)

        b6_1=Button(bg_img,text="Photos",cursor="hand2",command=self.open_img,font=("times new roman",15,"bold"),bg="blue",fg="white")
        b6_1.place(x=400,y=600,width=220,height=25)

        #Developer button
        img7=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\Developer-image.jpg")
        img7=img7.resize((220,220),Image.ANTIALIAS)
        self.photoimg7=ImageTk.PhotoImage(img7)

        b7=Button(bg_img,image=self.photoimg7,cursor="hand2",command=self.developer_data)
        b7.place(x=700,y=400,width=220,height=220)

        b7_1=Button(bg_img,text="Developer",cursor="hand2",command=self.developer_data,font=("times new roman",15,"bold"),bg="blue",fg="white")
        b7_1.place(x=700,y=600,width=220,height=25)


        #Exit button
        img8=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\exit1.jpg")
        img8=img8.resize((220,220),Image.ANTIALIAS)
        self.photoimg8=ImageTk.PhotoImage(img8)

        b8=Button(bg_img,image=self.photoimg8,cursor="hand2",command=self.iExit)
        b8.place(x=1000,y=400,width=220,height=220)

        b8_1=Button(bg_img,text="Exit",cursor="hand2",command=self.iExit,font=("times new roman",15,"bold"),bg="blue",fg="white")
        b8_1.place(x=1000,y=600,width=220,height=25)


    def open_img(self):
        os.startfile("D:\Major Project\Student Face Recognition Attendance System\data")


#************************Function buttons*****************
    def student_details(self):
        self.new_window=Toplevel(self.root)
        self.app=Student(self.new_window)

    def train_data(self):
        self.new_window=Toplevel(self.root)
        self.app=Train(self.new_window)

    def face_data(self):
        self.new_window=Toplevel(self.root)
        self.app=Face_Recognition(self.new_window)

    def attendance_data(self):
        self.new_window=Toplevel(self.root)
        self.app=Attendance(self.new_window)

    def developer_data(self):
        self.new_window=Toplevel(self.root)
        self.app=Developer(self.new_window)

    def help(self):
        self.new_window=Toplevel(self.root)
        self.app=Help(self.new_window)

    def iExit(self):
        self.iExit=tkinter.messagebox.askyesno("Face Recognition","Are you sure to exit this project",parent=self.root)
        if self.iExit >0:
          self.root.destroy()
        else:
            return

if __name__ == "__main__":
    root=Tk()
    object=Face_Recognition_System(root)
    root.mainloop()
