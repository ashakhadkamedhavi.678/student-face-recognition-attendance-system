from tkinter import*
from tkinter import ttk
from PIL import Image, ImageTk
from tkinter import messagebox
import mysql.connector
import cv2

class Student:
    def __init__(self, root):
        self.root=root
        self.root.geometry("1530x790+0+0")
        self.root.title("face Recognition System")
#**********variable****************
        self.var_dept=StringVar()
        self.var_course=StringVar()
        self.var_year=StringVar()
        self.var_semester=StringVar()
        self.var_std_id=StringVar()
        self.var_std_name=StringVar()
        self.var_div=StringVar()
        self.var_roll=StringVar()
        self.var_gender=StringVar()
        self.var_dob=StringVar()
        self.var_email=StringVar()
        self.var_phone=StringVar()
        self.var_address=StringVar()
        self.var_teacher=StringVar()
      

#Background Image
        img=Image.open(r"college_images\back.jpg")
        img=img.resize((1530,790),Image.ANTIALIAS)
        self.photoimg=ImageTk.PhotoImage(img)

        bg_img=Label(self.root,image=self.photoimg)
        bg_img.place(x=0,y=0,width=1530,height=790)

        title_lbl=Label(bg_img,text="Student Face Recognition Attendance System",font=("times new roman",25,"bold"),bg="white",fg="blue")
        title_lbl.place(x=(-80),y=20,width=1530,height=45)


       # main_frame=Frame(bg_img, bd=2, bg="white")
       #main_frame.place(x=10,y=80,width=1335,height=610)
        
        #Left lable frame
        Left_frame=LabelFrame(bg_img,bd=3,bg="skyblue",relief=RIDGE,text="Student Details",font=("times new roman",25,"bold"),fg="white")
        Left_frame.place(x=50,y=120,width=600,height=550)


        #Current course
        current_course_frame=LabelFrame(Left_frame,bd=2,bg="white",relief=RIDGE,text="Current Course Information",font=("times new roman",12,"bold" ))
        current_course_frame.place(x=20,y=10,width=550,height=120)

        #Department
        dep_lable=Label(current_course_frame,text="Department",font=("times new roman",12,"bold"))
        dep_lable.grid(row=0,column=0,padx=10,pady=10,sticky=W)

        dep_combo=ttk.Combobox(current_course_frame,textvariable=self.var_dept,font=("times new roman",12,"bold"),state="readonly",width=16)
        dep_combo["values"]=("Select Department","IT","Database","Business")
        dep_combo.current(0)
        dep_combo.grid(row=0,column=1,padx=10,pady=10,sticky=W)
       
        #Course
        course_lable=Label(current_course_frame,text="Course",font=("times new roman",12,"bold"))
        course_lable.grid(row=0,column=2,padx=10,pady=10,sticky=W)

        course_combo=ttk.Combobox(current_course_frame,textvariable=self.var_course,font=("times new roman",12,"bold"),state="readonly",width=16)
        course_combo["values"]=("Select Course","BCIS","BHM","BBA")
        course_combo.current(0)
        course_combo.grid(row=0,column=3,padx=10,pady=10,sticky=W)

        #Year
        year_lable=Label(current_course_frame,text="Year",font=("times new roman",12,"bold"))
        year_lable.grid(row=1,column=0,padx=10,pady=10,sticky=W)

        year_combo=ttk.Combobox(current_course_frame,textvariable=self.var_year,font=("times new roman",12,"bold"),state="readonly",width=16)
        year_combo["values"]=("Select Year","2020-21","2021-22","2022-21")
        year_combo.current(0)
        year_combo.grid(row=1,column=1,padx=10,pady=10,sticky=W)

        #Semester
        semester_lable=Label(current_course_frame,text="Semester",font=("times new roman",12,"bold"))
        semester_lable.grid(row=1,column=2,padx=10,pady=10,sticky=W)

        semester_combo=ttk.Combobox(current_course_frame,textvariable=self.var_semester,font=("times new roman",12,"bold"),state="readonly",width=16)
        semester_combo["values"]=("Select Semester","Semester-1","Semester-2")
        semester_combo.current(0)
        semester_combo.grid(row=1,column=3,padx=10,pady=10,sticky=W)


        #class_student_frame
    
        class_student_frame=LabelFrame(Left_frame,bd=2,bg="white",relief=RIDGE,text="Class Student Information",font=("times new roman",12,"bold" ))
        class_student_frame.place(x=20,y=150,width=550,height=350)


        #Student ID
        studentID_lable=Label(class_student_frame,text="Student Id:",font=("times new roman",12,"bold"),bg="white")
        studentID_lable.grid(row=0,column=0,padx=10,pady=10,sticky=W)
        
        studentID_entry=ttk.Entry(class_student_frame,textvariable=self.var_std_id,width=14,font=("times row roman",13,"bold"))
        studentID_entry.grid(row=0,column=1,pady=10,sticky=W)

        #Student Name
        student_Name_lable=Label(class_student_frame,text="Student Name:",font=("times new roman",12,"bold"),bg="white")
        student_Name_lable.grid(row=0,column=2,padx=10,pady=15,sticky=W)
        
        studentName_entry=ttk.Entry(class_student_frame,textvariable=self.var_std_name,width=13,font=("times row roman",13,"bold"))
        studentName_entry.grid(row=0,column=3,padx=10,pady=10,sticky=W)

        #Class Division
        student_div_lable=Label(class_student_frame,text="Class Division:",font=("times new roman",12,"bold"),bg="white")
        student_div_lable.grid(row=1,column=0,padx=10,pady=10,sticky=W)
        

        div_combo=ttk.Combobox(class_student_frame,textvariable=self.var_div,font=("times new roman",12,"bold"),state="readonly",width=13)
        div_combo["values"]=("Select Division","A","B","C")
        div_combo.current(0)
        div_combo.grid(row=1,column=1,pady=10,sticky=W)
       

        #Roll No
        roll_no_lable=Label(class_student_frame,text="Roll No:",font=("times new roman",12,"bold"),bg="white")
        roll_no_lable.grid(row=1,column=2,padx=10,pady=10,sticky=W)
        
        roll_no_entry=ttk.Entry(class_student_frame,textvariable=self.var_roll,width=13,font=("times row roman",13,"bold"))
        roll_no_entry.grid(row=1,column=3,padx=10,pady=10,sticky=W)

        #Gender
        Gender_lable=Label(class_student_frame,text="Gender:",font=("times new roman",12,"bold"),bg="white")
        Gender_lable.grid(row=2,column=0,padx=10,pady=10,sticky=W)
        
        gender_combo=ttk.Combobox(class_student_frame,textvariable=self.var_gender,font=("times new roman",12,"bold"),state="readonly",width=13)
        gender_combo["values"]=("Male","Female","Other")
        gender_combo.current(0)
        gender_combo.grid(row=2,column=1,pady=10,sticky=W)
       

        #DOB
        dob_lable=Label(class_student_frame,text="DOB:",font=("times new roman",12,"bold"),bg="white")
        dob_lable.grid(row=2,column=2,padx=10,pady=10,sticky=W)
        
        dob_entry=ttk.Entry(class_student_frame,textvariable=self.var_dob,width=13,font=("times row roman",13,"bold"))
        dob_entry.grid(row=2,column=3,padx=10,pady=10,sticky=W)

        #Email
        email_lable=Label(class_student_frame,text="Email:",font=("times new roman",12,"bold"),bg="white")
        email_lable.grid(row=3,column=0,padx=10,pady=10,sticky=W)
        
        email_entry=ttk.Entry(class_student_frame,textvariable=self.var_email,width=14,font=("times row roman",13,"bold"))
        email_entry.grid(row=3,column=1,pady=10,sticky=W)

        #Phone no
        phone_lable=Label(class_student_frame,text="Phone No:",font=("times new roman",12,"bold"),bg="white")
        phone_lable.grid(row=3,column=2,padx=10,pady=10,sticky=W)
        
        phone_entry=ttk.Entry(class_student_frame,textvariable=self.var_phone,width=13,font=("times row roman",13,"bold"))
        phone_entry.grid(row=3,column=3,padx=10,pady=10,sticky=W)

        #Address
        address_lable=Label(class_student_frame,text="Address:",font=("times new roman",12,"bold"),bg="white")
        address_lable.grid(row=4,column=0,padx=10,pady=10,sticky=W)
        
        address_entry=ttk.Entry(class_student_frame,textvariable=self.var_address,width=14,font=("times row roman",13,"bold"))
        address_entry.grid(row=4,column=1,pady=10,sticky=W)

        #Teacher Name
        teacher_name_lable=Label(class_student_frame,text="Teacher Name:",font=("times new roman",12,"bold"),bg="white")
        teacher_name_lable.grid(row=4,column=2,padx=10,pady=10,sticky=W)
        
        teacher_name_entry=ttk.Entry(class_student_frame,textvariable=self.var_teacher,width=13,font=("times row roman",13,"bold"))
        teacher_name_entry.grid(row=4,column=3,padx=10,pady=10,sticky=W)

        #Radio button
        self.var_radio1=StringVar()
        radiobtn1=ttk.Radiobutton(class_student_frame,variable=self.var_radio1,text="Take Photo Sample",value="Yes")
        radiobtn1.grid(row=6,column=0)

    
        radiobtn2=ttk.Radiobutton(class_student_frame,variable=self.var_radio1,text="No Photo Sample",value="No")
        radiobtn2.grid(row=6,column=1)


        #button frame
        btn_frame=Frame(class_student_frame,bd=2,relief=RIDGE)
        btn_frame.place(x=0,y=260,width=550,height=60)

        save_btn=Button(btn_frame,text="save",command=self.add_data,width=13,font=("times new roman",13,"bold"),bg="blue",fg="white")
        save_btn.grid(row=0,column=0)

        update_btn=Button(btn_frame,text="update",width=13,command=self.update_data,font=("times new roman",13,"bold"),bg="blue",fg="white")
        update_btn.grid(row=0,column=1)

        delete_btn=Button(btn_frame,text="delete",width=13,command=self.delete_data,font=("times new roman",13,"bold"),bg="blue",fg="white")
        delete_btn.grid(row=0,column=2)

        reset_btn=Button(btn_frame,text="reset",width=12,command=self.reset_data,font=("times new roman",13,"bold"),bg="blue",fg="white")
        reset_btn.grid(row=0,column=3)

        btn_frame1=Frame(class_student_frame,bd=2,relief=RIDGE)
        btn_frame1.place(x=0,y=290,width=550,height=35)

        take_photo_btn=Button(btn_frame1,command=self.generate_dataset,text="take photo",width=54,font=("times new roman",13,"bold"),bg="blue",fg="white")
        take_photo_btn.grid(row=0,column=0)

 

        #Right lable frame
        Right_frame=LabelFrame(bg_img,bd=3,bg="skyblue",relief=RIDGE,text="Student Details",font=("times new roman",25,"bold"),fg="white")
        Right_frame.place(x=700,y=120,width=600,height=550)

        #Search
        search_frame=LabelFrame(Right_frame,bd=2,bg="white",relief=RIDGE,text="Search",font=("times new roman",12,"bold"))
        search_frame.place(x=20,y=10,width=550,height=80)

        search_by_lable=Label(search_frame,text="Search By:",font=("times new roman",12,"bold"),bg="white")
        search_by_lable.grid(row=0,column=0,padx=10,pady=10,sticky=W)

        self.var_combo_search=StringVar()
        search_combo=ttk.Combobox(search_frame,textvariable=self.var_combo_search,font=("times new roman",12,"bold"),state="readonly",width=13)
        search_combo["values"]=("Select","Roll No","Phone No","Student Name")
        search_combo.current(0)
        search_combo.grid(row=0,column=1,padx=5,pady=10,sticky=W)  

        self.var_search=StringVar()
        search_entry=ttk.Entry(search_frame,textvariable=self.var_search,width=13,font=("times row roman",13,"bold"))
        search_entry.grid(row=0,column=2,padx=5,pady=10,sticky=W)

        search_btn=Button(search_frame,command=self.search_data,text="search",width=7,font=("times new roman",13,"bold"),bg="blue",fg="white")
        search_btn.grid(row=0,column=3)

        show_all_btn=Button(search_frame,command=self.fetch_data,text="show all",width=7,font=("times new roman",13,"bold"),bg="blue",fg="white")
        show_all_btn.grid(row=0,column=4, padx=5)

        #table frame
        table_frame=Frame(Right_frame,bd=2,bg="white",relief=RIDGE)
        table_frame.place(x=20,y=110,width=550,height=390)

        scroll_x=ttk.Scrollbar(table_frame,orient=HORIZONTAL)
        scroll_y=ttk.Scrollbar(table_frame,orient=VERTICAL)

        self.student_table=ttk.Treeview(table_frame,column=("dept","course","year","sem","id","name","div","roll","gender","dob","email","phone","address","teacher","photo"),xscrollcommand=scroll_x.set,yscrollcommand=scroll_y.set)
        scroll_x.pack(side=BOTTOM,fill=X)
        scroll_y.pack(side=RIGHT,fill=Y)
        scroll_x.config(command=self.student_table.xview)
        scroll_y.config(command=self.student_table.yview)

        self.student_table.heading("dept",text="Department")
        self.student_table.heading("course",text="Course")
        self.student_table.heading("year",text="Year")
        self.student_table.heading("sem",text="Semester")
        self.student_table.heading("id",text="StudentId")
        self.student_table.heading("name",text="Name")
        self.student_table.heading("div",text="Division")
        self.student_table.heading("roll",text="Roll No")  
        self.student_table.heading("gender",text="Gender") 
        self.student_table.heading("dob",text="DOB")
        self.student_table.heading("email",text="Email")
        self.student_table.heading("phone",text="Phone")
        self.student_table.heading("address",text="Address")
        self.student_table.heading("teacher",text="Teacher")
        self.student_table.heading("photo",text="PhotoSampleStatus")
        self.student_table["show"]="headings"

        
        self.student_table.column("dept",width=100)
        self.student_table.column("course",width=100)
        self.student_table.column("year",width=100)
        self.student_table.column("sem",width=100)
        self.student_table.column("id",width=100)
        self.student_table.column("name",width=100)
        self.student_table.column("div",width=100)
        self.student_table.column("roll",width=100)
        self.student_table.column("gender",width=100)
        self.student_table.column("dob",width=100)  
        self.student_table.column("email",width=100)
        self.student_table.column("phone",width=100)
        self.student_table.column("address",width=100)
        self.student_table.column("teacher",width=100) 
        self.student_table.column("photo",width=100)
        
        self.student_table.pack(fill=BOTH,expand=1)
        self.student_table.bind("<ButtonRelease>",self.get_cursor)
        self.fetch_data()
#************************Function*****************
    def add_data(self):
        if self.var_dept.get()=="Select Department" or self.var_std_name.get()=="" or self.var_std_id.get()=="":
            messagebox.showerror("Error","All Fields are required",parent=self.root)
        else:
            try:
                conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
                my_cursor=conn.cursor()
                my_cursor.execute("insert into student values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(
                                                                                                                self.var_dept.get(),
                                                                                                                self.var_course.get(),
                                                                                                                self.var_year.get(),
                                                                                                                self.var_semester.get(),
                                                                                                                self.var_std_id.get(),
                                                                                                                self.var_std_name.get(),
                                                                                                                self.var_div.get(),
                                                                                                                self.var_roll.get(),
                                                                                                                self.var_gender.get(),
                                                                                                                self.var_dob.get(),
                                                                                                                self.var_email.get(),
                                                                                                                self.var_phone.get(),
                                                                                                                self.var_address.get(),
                                                                                                                self.var_teacher.get(),
                                                                                                                self.var_radio1.get() 
                                                                                                           ))
                conn.commit()
                self.fetch_data()
                conn.close()
                messagebox.showinfo("Success","Student details has been added successfully",parent=self.root)

            except Exception as es:
                messagebox.showerror("Error", f"Due to:{str(es)}",parent=self.root)

#**********************Fetch Data*********************
    def fetch_data(self):
        conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
        my_cursor=conn.cursor()
        my_cursor.execute("select * from student")
        data=my_cursor.fetchall()

        if len(data)!=0:
            self.student_table.delete(*self.student_table.get_children())
            for i in data:
                self.student_table.insert("",END,values=i)
            conn.commit()
        conn.close()


#******************get cursor*****************     
    def get_cursor(self,event=""):
        cursor_focus=self.student_table.focus()
        content=self.student_table.item(cursor_focus)
        data=content["values"]

        self.var_dept.set(data[0]),
        self.var_course.set(data[1]),
        self.var_year.set(data[2]), 
        self.var_semester.set(data[3]),
        self.var_std_id.set(data[4]),  
        self.var_std_name.set(data[5]),  
        self.var_div.set(data[6]),  
        self.var_roll.set(data[7]),  
        self.var_gender.set(data[8]),  
        self.var_dob.set(data[9]),  
        self.var_email.set(data[10]),  
        self.var_phone.set(data[11]), 
        self.var_address.set(data[12]),  
        self.var_teacher.set(data[13]), 
        self.var_radio1.set(data[14])  
#******************update function********************
    def update_data(self):
        if self.var_dept.get()=="Select Department" or self.var_std_name.get()=="" or self.var_std_id.get()=="":
            messagebox.showerror("Error","All Fields are required",parent=self.root)
        else:
            try:
                Upadate=messagebox.askyesno("Update","Do you want to update this student details",parent=self.root)
                if Upadate>0:
                    conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
                    my_cursor=conn.cursor()
                    my_cursor.execute("update student set Dept=%s,Course=%s,Year=%s,Semester=%s,Name=%s,Division=%s,Roll=%s,Gender=%s,Dob=%s,Email=%s,Phone=%s,Address=%s,Teacher=%s,PhotoSample=%s where Student_id=%s",(
                                                                                                                                                                                                                    self.var_dept.get(),
                                                                                                                                                                                                                    self.var_course.get(),
                                                                                                                                                                                                                    self.var_year.get(),
                                                                                                                                                                                                                    self.var_semester.get(), 
                                                                                                                                                                                                                    self.var_std_name.get(),
                                                                                                                                                                                                                    self.var_div.get(),
                                                                                                                                                                                                                    self.var_roll.get(),
                                                                                                                                                                                                                    self.var_gender.get(),
                                                                                                                                                                                                                    self.var_dob.get(),
                                                                                                                                                                                                                    self.var_email.get(),
                                                                                                                                                                                                                    self.var_phone.get(),
                                                                                                                                                                                                                    self.var_address.get(),
                                                                                                                                                                                                                    self.var_teacher.get(),
                                                                                                                                                                                                                    self.var_radio1.get(),
                                                                                                                                                                                                                    self.var_std_id.get(), 
                                                                                                                                                                                                            ))


                else:
                    if not Upadate:
                        return
                messagebox.showerror("Success","Student details successfully update completed",parent=self.root)
                conn.commit()
                conn.fetch_data()
                conn.close()
            except Exception as es:
                messagebox.showerror("Error", f"Due To:{str(es)}",parent=self.root)

#******************delete fuction******************* 
    def delete_data(self):
        if self.var_std_id.get()=="":
            messagebox.showerror("Error","Student id must be required",parent=self.root)
        else:
            try:
                delete=messagebox.askyesno("Student Delete Page", "Do you want to delete this student",parent=self.root)
                if delete>0:
                    conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
                    my_cursor=conn.cursor()
                    sql="delete from student where student_id=%s"
                    val=(self.var_std_id.get(),)
                    my_cursor.execute(sql,val)
                else:
                    if not delete:
                        return
                    
                conn.commit()
                self.fetch_data()
                conn.close()
                messagebox.showinfo("Delete","Successfully deleted student details",parent=self.root)
            except Exception as es:
                messagebox.showerror("Error", f"Due To:{str(es)}",parent=self.root)

#**************reset**************************

    def reset_data(self):
        self.var_dept.set("Select Department")
        self.var_course.set("Select Course")
        self.var_year.set("Select Year")
        self.var_semester.set("Select Semester")
        self.var_std_id.set("")
        self.var_std_name.set("")
        self.var_div.set("Select Division")
        self.var_roll.set("")
        self.var_gender.set("Male")
        self.var_dob.set("")
        self.var_email.set("")
        self.var_phone.set("")
        self.var_address.set("")
        self.var_teacher.set("")
        self.var_radio1.set("")
        


#************************Generate data set or Take photo sample********************

    def generate_dataset(self):
        if self.var_dept.get()=="Select Department" or self.var_std_name.get()=="" or self.var_std_id.get()=="":
            messagebox.showerror("Error","All Fields are required",parent=self.root)
        else:
            try:
                conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
                my_cursor=conn.cursor() 
                my_cursor.execute("select * from student")
                myresult=my_cursor.fetchall()
                id=0
                for x in myresult:
                    id+=1
                    my_cursor.execute("update student set Dept=%s,Course=%s,Year=%s,Semester=%s,Name=%s,Division=%s,Roll=%s,Gender=%s,Dob=%s,Email=%s,Phone=%s,Address=%s,Teacher=%s,PhotoSample=%s where Student_id=%s",(
                                                                                                                                                                                                                    self.var_dept.get(),
                                                                                                                                                                                                                    self.var_course.get(),
                                                                                                                                                                                                                    self.var_year.get(),
                                                                                                                                                                                                                    self.var_semester.get(), 
                                                                                                                                                                                                                    self.var_std_name.get(),
                                                                                                                                                                                                                    self.var_div.get(),
                                                                                                                                                                                                                    self.var_roll.get(),
                                                                                                                                                                                                                    self.var_gender.get(),
                                                                                                                                                                                                                    self.var_dob.get(),
                                                                                                                                                                                                                    self.var_email.get(),
                                                                                                                                                                                                                    self.var_phone.get(),
                                                                                                                                                                                                                    self.var_address.get(),
                                                                                                                                                                                                                    self.var_teacher.get(),
                                                                                                                                                                                                                    self.var_radio1.get(),
                                                                                                                                                                                                                    self.var_std_id.get()==id+1 
                                                                                                                                                                                                            ))
                conn.commit()
                self.fetch_data()
                self.reset_data()
                conn.close()
            #load predefined data on face frontals from opencv
                face_classifer=cv2.CascadeClassifier("D:\Major Project\Student Face Recognition Attendance System\haarcascade_frontalface_default.xml")

                def face_cropped(img):
                    gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
                    faces=face_classifer.detectMultiScale(gray,1.3,5)
                    #Scaling facetor=1.3
                    #Minimum Neighbor=5

                    for(x,y,w,h) in faces:
                        face_cropped=img[y:y+h,x:x+w]
                        return face_cropped

                cap=cv2.VideoCapture(0)
                img_id=0
                while True:
                    ret,my_frame=cap.read()
                    if face_cropped(my_frame) is not None:
                        img_id+=1
                        face=cv2.resize(face_cropped(my_frame),(450,450))
                        face=cv2.cvtColor(face,cv2.COLOR_BGR2GRAY)
                        file_name_path="data/user."+str(id)+"."+str(img_id)+".jpg"
                        cv2.imwrite(file_name_path,face)
                        cv2.putText(face,str(img_id),(50,50),cv2.FONT_HERSHEY_COMPLEX,2,(0,255,0),2)
                        cv2.imshow("Cropped Face",face)

                    if cv2.waitKey(1)==13 or int(img_id)==100:
                        break
                cap.release()
                cv2.destroyAllWindows()
                messagebox.showinfo("Result","Generating data sets compled!!!")
            except Exception as es:
                messagebox.showerror("Error", f"Due To:{str(es)}",parent=self.root)


#***********search data*************
    def search_data(self):
        if self.var_combo_search.get()=="" or self.var_search.get()=="":
            messagebox.showerror("Error","Please select option")
        else:
            try:
                conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
                my_cursor=conn.cursor() 
             
                my_cursor.execute("select * from student where " +str(self.var_combo_search.get())+" LIKE '%"+str(self.var_search.get())+"%'")
                rows=my_cursor.fetchall()

                if len(rows)!=0:
                    self.student_table.delete(*self.student_table.get_children())
                    for i in rows:
                        self.student_table.insert("",END,values=i)
                    conn.commit()
                conn.close()

            except Exception as es:
                messagebox.showerror("Error", f"Due To :{str(es)}",parent=self.root)


            


if __name__ == "__main__":
    root=Tk()
    object=Student(root)
    root.mainloop()