from tkinter import*
from tkinter import ttk
from PIL import Image, ImageTk
from tkinter import messagebox
import mysql.connector
import cv2
import os
import numpy as np


class Train:
    def __init__(self, root):
        self.root=root
        self.root.geometry("1530x790+0+0")
        self.root.title("face Recognition System")

        title_lbl=Label(self.root,text="TRAIN DATA SET",font=("times new roman",25,"bold"),bg="white",fg="blue")
        title_lbl.place(x=(-50),y=20,width=1530,height=45)


   
        img_top=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\blackbackground.png")
        img_top=img_top.resize((1400,700),Image.ANTIALIAS)
        self.photoimg_top=ImageTk.PhotoImage(img_top)

        f_lbl=Label(self.root,image=self.photoimg_top)
        f_lbl.place(x=0,y=80,width=1400,height=700)

        #button
        b1_1=Button(self.root,text="Train Data",command=self.train_classifier,width=13,font=("times new roman",30,"bold"),bg="blue",fg="white")
        b1_1.place(x=0,y=660,width=1530,height=40)

    def train_classifier(self):
        data_dir=(r"D:\Major Project\Student Face Recognition Attendance System\data")
        path=[os.path.join(data_dir,file) for file in os.listdir(data_dir)]

            
        faces=[]
        ids=[]

        for image in path:
            img=Image.open(image).convert('L') #Gray scale image
            imageNp=np.array(img,'uint8')
            id=int(os.path.split(image)[1].split('.')[1])

            faces.append(imageNp)
            ids.append(id)
            cv2.imshow("Training", imageNp)
            cv2.waitKey(1)==13
        ids=np.array(ids)

            #****************Train the classifier and save*********
        clf=cv2.face.LBPHFaceRecognizer_create()
        clf.train(faces,ids)
        clf.write("D:\Major Project\Student Face Recognition Attendance System\classifier.xml")
        cv2.destroyAllWindows()
        messagebox.showinfo("Result","Training datasets completed!!")


if __name__ == "__main__":
    root=Tk()
    object=Train(root)
    root.mainloop()