from msilib.schema import File
from tkinter import*
from tkinter import ttk
from PIL import Image,ImageTk
from tkinter import messagebox
import mysql.connector


class Register:
    def __init__(self,root):
        self.root=root
        self.root.title("Register")
        self.root.geometry("1530x790+0+0")

    #**************variable*****
        self.var_fname=StringVar()
        self.var_lname=StringVar()
        self.var_contact=StringVar()
        self.var_email=StringVar()
        self.var_security_q=StringVar()
        self.var_security_a=StringVar()
        self.var_password=StringVar()
        self.var_confirm_password=StringVar()
        

#************bg image*********
        img_top=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\pexels-photo-1303081.jpeg")
        img_top=img_top.resize((1530,700),Image.ANTIALIAS)
        self.photoimg_top=ImageTk.PhotoImage(img_top)

        f_lbl=Label(self.root,image=self.photoimg_top)
        f_lbl.place(x=0,y=0,width=1530,height=720)



        #*******left image*************
        left_img=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\flower.webp")
        left_img=left_img.resize((470,550),Image.ANTIALIAS)
        self.photoleft_img=ImageTk.PhotoImage(left_img)

        f_lbl=Label(self.root,image=self.photoleft_img)
        f_lbl.place(x=50,y=100,width=400,height=550)


        
        #frame
        main_frame=Frame(self.root, bg="white",relief=RIDGE)
        main_frame.place(x=450,y=100,width=600,height=550)

         
        register_lbl=Label(main_frame,text="Register",font=("times new roman",20,"bold"),fg='purple')
        register_lbl.place(x=250,y=10)



        #***label and entry**********
        fname_label=Label(main_frame,text="Firstname",font=("times new roman",15,"bold"),fg='purple')
        fname_label.place(x=10,y=50)

        self.fname=ttk.Entry(main_frame,textvariable=self.var_fname,font=("times new roman",15,"bold"))
        self.fname.place(x=10,y=80,width=250)


        lname_label=Label(main_frame,text="Lastname",font=("times new roman",15,"bold"),fg='purple')
        lname_label.place(x=340,y=50)

        self.lname=ttk.Entry(main_frame,textvariable=self.var_lname,font=("times new roman",15,"bold"))
        self.lname.place(x=340,y=80,width=250)



    
        contact_label=Label(main_frame,text="Contact",font=("times new roman",15,"bold"),fg='purple')
        contact_label.place(x=10,y=140)

        self.contact=ttk.Entry(main_frame,textvariable=self.var_contact,font=("times new roman",15,"bold"))
        self.contact.place(x=10,y=170,width=250)


        
        email_label=Label(main_frame,text="Email",font=("times new roman",15,"bold"),fg='purple')
        email_label.place(x=340,y=140)

        self.email=ttk.Entry(main_frame,textvariable=self.var_email,font=("times new roman",15,"bold"))
        self.email.place(x=340,y=170,width=250)



        
        security_q_label=Label(main_frame,text="Select Security Question",font=("times new roman",15,"bold"),fg='purple')
        security_q_label.place(x=10,y=230)

        self.combo_security_q=ttk.Combobox(main_frame,textvariable=self.var_security_q,font=("times new roman",15,"bold"),state="randomly")
        self.combo_security_q["values"]=("Select","Your Birth Place","Your Pet Name")
        self.combo_security_q.place(x=10,y=260,width=250)
        self.combo_security_q.current(0)


        
        security_a_label=Label(main_frame,text="Security Answer",font=("times new roman",15,"bold"),fg='purple')
        security_a_label.place(x=340,y=230)

        self.security_a=ttk.Entry(main_frame,textvariable=self.var_security_a,font=("times new roman",15,"bold"))
        self.security_a.place(x=340,y=260,width=250)



        
        password_label=Label(main_frame,text="Password",font=("times new roman",15,"bold"),fg='purple')
        password_label.place(x=10,y=320)

        self.password=ttk.Entry(main_frame,textvariable=self.var_password,font=("times new roman",15,"bold"))
        self.password.place(x=10,y=350,width=250)


        
        confirm_password_label=Label(main_frame,text="Confirm Password",font=("times new roman",15,"bold"),fg='purple')
        confirm_password_label.place(x=340,y=320)

        self.confirm_password=ttk.Entry(main_frame,textvariable=self.var_confirm_password,font=("times new roman",15,"bold"))
        self.confirm_password.place(x=340,y=350,width=250)

        #**********checkbutton************
        self.var_checkbtn=IntVar()
        checkbtn=Checkbutton(main_frame,variable=self.var_checkbtn,text="I Agree The Terms & Condition", font=("times new roman",10,"bold"),fg='purple',onvalue=1,offvalue=0)
        checkbtn.place(x=10,y=390)


        #*********buttons****************
        img=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\register-now-button.jpg")
        img=img.resize((200,50),Image.ANTIALIAS)
        self.photoimage=ImageTk.PhotoImage(img)
        b1=Button(main_frame,image=self.photoimage,command=self.register_data,borderwidth=0,cursor="hand2",font=("times new roman",15,"bold"))
        b1.place(x=70,y=440,width=200)


        img1=Image.open(r"D:\Major Project\Student Face Recognition Attendance System\college_images\login-now.jpg")
        img1=img1.resize((200,70),Image.ANTIALIAS)
        self.photoimage1=ImageTk.PhotoImage(img1)
        b2=Button(main_frame,image=self.photoimage1,borderwidth=0,cursor="hand2",font=("times new roman",15,"bold"))
        b2.place(x=290,y=450,width=210)

        #***************function declaration*************

    def register_data(self):
        if self.var_fname.get()==""or self.var_email.get()==""or self.var_security_q.get()=="Select":
            messagebox.showerror("Error","All fields are required")
        elif self.var_password.get()!=self.var_confirm_password.get():
            messagebox.showerror("Error","password & confirm password must be same")
        elif self.var_checkbtn.get()==0:
            messagebox.showerror("Error","Please agree our terms & condition")
        else:
            conn=mysql.connector.connect(host="localhost", username="roots",password="root", database="student_face_recognizer")
            my_cursor=conn.cursor() 
            query=("select * from register where email=%s")
            value=(self.var_email.get(),)
            my_cursor.execute(query,value)
            row=my_cursor.fetchone()
            if row!=None:
                messagebox.showerror("Error","User already exist, please try another email")
            else:
                my_cursor.execute("insert into register values(%s,%s,%s,%s,%s,%s,%s)",(
                                                                                        self.var_fname.get(),
                                                                                        self.var_lname.get(),
                                                                                        self.var_contact.get(),
                                                                                        self.var_email.get(),
                                                                                        self.var_security_q.get(),
                                                                                        self.var_security_a.get(),
                                                                                        self.var_password.get()
                                                                                    ))
            conn.commit()
            conn.close()
            messagebox.showinfo("Success","Register Successfully")
if __name__ == "__main__":
    root=Tk()
    object=Register(root)
    root.mainloop()